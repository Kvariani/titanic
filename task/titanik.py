import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    answer = []

    for title in ['Mr.', 'Mrs.', 'Miss.']:
        title_data = df[df['Name'].str.contains(title)]

        missing_count = title_data['Age'].isnull().sum()
        median_value = round(title_data['Age'].median())

        answer.append((title, missing_count, median_value))

    return answer
